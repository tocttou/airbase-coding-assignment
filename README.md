# Airbase Coding Assignment

Demo: http://airbase-request.surge.sh	

Run: `npm start -s`

Code: visit the `src/` directory

Total time taken: 6.2 hours

![desktop](./airbase.png)


![mobile](./airbase-mobile.png)

Key Points:

1. I have used TS for the project (create-react-app template) (reads just like JS). This allows me to get full type-safety without defining the prop-types for individual components (just have to type the data at the top level).
2. The data can be changed by changing the `request` prop for `RequestApproval` in `App.tsx`.
3. The data definition is available at `src/data.d.ts`.
4. The CSS for a component resides next to its declaration (`index.ts`) in an `index.css` file.
5. Using `flex-basis` to make the app responsive.

Stretch goals:
1. I wanted to use SASS (mostly for the variables but decided to write vanilla CSS instead).

Assumptions:

1. None of the keys in the data can be null. A `request` object not adhereing to the schema in `data.d.ts` might crash the app.
2. I figured that the `profile_picture` for an approver might not be present though (as seen in the Figma mockup), I have added a check for that.
3. It is not clear on what condition the warning message at the bottom must be shown, and if the buttons should be enabled/disabled. I have kept this part static.
4. If the status for an approver is not `accepted` (Approved), or `created` (Pending), it is assumed to be Denied.
