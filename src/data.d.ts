export namespace Data {
  interface Service {
    name: string;
    website: string;
    usage_count: number;
    logo: string;
    description: string;
  }

  interface RequestedBy {
    email: string;
    first_name: string;
    last_name: string;
    profile_picture: string;
  }

  interface Approver {
    approver: {
      email: string;
      first_name: string;
      last_name: string;
      profile_picture?: string;
    };
    status: string;
    last_notified_time: string;
    created_date: string;
    last_updated_date: string;
  }

  interface Request {
    service: Service;
    requested_by: RequestedBy;
    id: number;
    cost_cents: number;
    renewal_frequency_in_months: number;
    spend_limit_cents: number;
    spend_limit_window: string;
    card_lock_date: null;
    approvers: Approver[];
    description: string;
    status: string;
    expense_account: string;
    created_date: string;
    last_updated_date: string;
    cost: number;
    spend_limit: number;
    files: string[];
  }
}
