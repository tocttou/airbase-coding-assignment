const fileNameRegex = /(\w+)(\.\w+)+(?!.*(\w+)(\.\w+)+)/;

export const getFileNameFromURL = (url: string) => {
  const matches = url.match(fileNameRegex);
  return matches ? matches[0] : null;
};

export const getFormattedDateString = (date: string) => {
  const options = { weekday: "long", year: "numeric", month: "long", day: "numeric" };
  return new Date(date).toLocaleDateString("en-US", options);
};
