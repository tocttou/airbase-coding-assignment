import React, { useMemo } from "react";
import { Data } from "../data";
import RequestDetails from "./request-details";
import Approver from "./approver";
import "./index.css";

const RequestApproval = (props: { request: Data.Request }) => {
  const { service, id, approvers } = props.request;

  const acceptedApprovers = useMemo(() => approvers.filter(x => x.status === "accepted"), [
    approvers
  ]);
  const pendingApprovers = useMemo(() => approvers.filter(x => x.status === "created"), [
    approvers
  ]);
  const deniedApprovers = useMemo(
    () => approvers.filter(x => x.status !== "accepted" && x.status !== "created"),
    [approvers]
  );

  return (
    <div className="approval-wrapper">
      <div className="heading" title={service.description}>
        <img src={service.logo} alt="service-logo" />
        <span>{`Request for ${service.name} (#${id})`}</span>
      </div>
      <div className="section section-left">
        <RequestDetails request={props.request} />
        <div className="request-warning">
          <div className="red">
            Your company is already paying for Amazon Web Service on a recurring basis.
          </div>
          <div>(1 instance owned by John Smith).</div>
        </div>
      </div>
      <div className="section section-right">
        <Approver approvers={acceptedApprovers} heading="Approved" />
        <Approver approvers={deniedApprovers} heading="Denied" />
        <Approver approvers={pendingApprovers} heading="Pending" />
      </div>
      <div className="section section-left">
        <div className="request-actions">
          <button className="button button-green">Approve</button>
          <button className="button button-red">Deny</button>
        </div>
      </div>
    </div>
  );
};

export default RequestApproval;
