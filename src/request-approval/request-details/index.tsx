import React from "react";
import { Data } from "../../data";
import { getFileNameFromURL } from "../../utils";
import "./index.css";

const RequestDetails = (props: { request: Data.Request }) => {
  const {
    requested_by,
    cost_cents,
    renewal_frequency_in_months,
    expense_account,
    files,
    description
  } = props.request;
  return (
    <div className="request-details">
      <div className="details-line">
        <div>Requested by</div>
        <div className="requested-by">
          <img src={requested_by.profile_picture} alt="requested-by" />
          <span>
            {requested_by.first_name} {requested_by.last_name}
          </span>
        </div>
      </div>
      <div className="details-line">
        <div>Cost</div>
        <div>${(cost_cents / 100).toFixed(2)}</div>
      </div>
      <div className="details-line details-line-half">
        <div>Renewal Frequency</div>
        <div>
          {renewal_frequency_in_months} {renewal_frequency_in_months > 1 ? "months" : "month"}
        </div>
      </div>
      <div className="details-line details-line-half">
        <div>Annual Cost</div>
        <div>${((cost_cents * 12) / 100).toFixed(2)}</div>
      </div>
      <div className="details-line">
        <div>Expense Account</div>
        <div>{expense_account}</div>
      </div>
      <div className="details-line">
        <div>File</div>
        <div>
          {files.map(url => (
            <div className="file-dl-wrapper" key={url}>
              <img src={require("../../assets/file.svg")} alt="file-download" />
              <a href={url} target="_blank" rel="noopener noreferrer">
                {getFileNameFromURL(url)}
              </a>
            </div>
          ))}
        </div>
      </div>
      <div className="details-line">
        <div>Description</div>
        <div className="description">{description}</div>
      </div>
    </div>
  );
};

export default RequestDetails;
