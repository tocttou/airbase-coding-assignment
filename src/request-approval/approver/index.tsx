import React from "react";
import { getFormattedDateString } from "../../utils";
import { Data } from "../../data";
import "./index.css";

const Approver = (props: { approvers: Data.Approver[]; heading: string }) => {
  const { approvers, heading } = props;
  if (approvers.length === 0) {
    return null;
  }
  return (
    <div className="approvers">
      <div className="approval-type">{heading}</div>
      <div className="approval-list">
        {approvers.map(x => {
          const { approver, last_updated_date, last_notified_time, status } = x;
          const { email, profile_picture, first_name, last_name } = approver;
          let approverDate = null;
          let statusMark = <div className="circle circle-empty" />;
          if (status === "accepted") {
            approverDate = (
              <div className="approver-date">
                Approved {getFormattedDateString(last_updated_date)}
              </div>
            );
            statusMark = (
              <div className="circle circle-tick">
                <img src={require("../../assets/tick.svg")} alt="tick-mark" />
              </div>
            );
          } else {
            approverDate = (
              <div className="approver-date">
                Last Notified {getFormattedDateString(last_notified_time)}
              </div>
            );
          }
          if (status !== "accepted" && status !== "created") {
            statusMark = <div className="circle circle-red" />;
          }
          return (
            <div className="approval-list-item" key={email}>
              <div>
                <div className="circle" />
              </div>
              {profile_picture ? <img src={profile_picture} alt="approver-pic" /> : <div />}
              <div className="item-flex-width">
                <div className="approver-info">
                  <span>
                    {first_name} {last_name}
                  </span>
                  <span>{email}</span>
                </div>
                {approverDate}
              </div>
              <div>{statusMark}</div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default Approver;
